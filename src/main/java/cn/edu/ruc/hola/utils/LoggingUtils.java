package cn.edu.ruc.hola.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggingUtils {
    static {
        File f = new File("./log");
        if (!f.exists()) f.mkdir();
        logger = Logger.getLogger("hola");
        env();
    }

    private static void env() {
        StringBuilder logPath = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        logPath.append("./log/");
        logPath.append(sdf.format(new Date())).append(".log");
        try {
            FileHandler fileHandler = new FileHandler(logPath.toString(), true);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final Logger logger;

    public static Logger logger() {
        return logger;
    }
}
