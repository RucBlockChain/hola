package cn.edu.ruc.hola.models.couchdb

class Doc {
    lateinit var _id: String
    lateinit var _rev: String

    var code = -1
    lateinit var message: String
    lateinit var data: Data

    /**
     * custom specific
     */
    class Data {
        var id = 0
        lateinit var name: String
        lateinit var bfirstletter: String
        lateinit var logo: String
        lateinit var country: String
        var countryid = -1
        override fun toString(): String {
            return "Data(id=$id, name='$name', bfirstletter='$bfirstletter', logo='$logo', country='$country', countryid=$countryid)"
        }

    }

    override fun toString(): String {
        return "Doc(_id='$_id', _rev='$_rev', code=$code, message='$message', data=$data)"
    }
}