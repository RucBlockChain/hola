package cn.edu.ruc.hola.service

enum class ServiceType {
    TYPE_COUCHDB, TYPE_MYSQL
}