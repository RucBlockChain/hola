package cn.edu.ruc.hola.fake

object FakeConfig {
    // base fake database name
    const val databaseName: String = "fake"
}