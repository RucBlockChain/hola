package cn.edu.ruc.hola.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BuddyUtils2Test {

    public static class Call {
        public String call1(String ok) {
            System.out.println(" call 1 call hello " + ok);
            return "";
        }

        public String call2() {
            System.out.println(" call 2 ");
            return "hello world";
        }
    }

    @Test
    Call getInstance() {
        var c = BuddyUtils2.getInstance(new Call(), Call.class);
        assertNotNull(c);
        return c;
    }

    @Test
    void call() {
        final var call = getInstance();
        call.call1("ok");
    }
}